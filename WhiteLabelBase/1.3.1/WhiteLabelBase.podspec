Pod::Spec.new do |s|
    s.name         = "WhiteLabelBase"
    s.version      = "1.3.1"
    s.summary      = "WhiteLabelBase - TravelStorys"
    s.description  = "White label app overrides for the TravelStorys SDK."
    s.homepage     = "https://travelstorys.com"
    s.license = { :type => 'Copyright', :text => <<-LICENSE
                   Copyright 2019 TravelStorysGPS, LLC
                   Permission is granted to use, but not modify the code in any projects where the user has an existing license agreement with TravelStorysGPS, LLC
                  LICENSE
                }
    s.author             = { "TravelStorys" => "info@travelstorysgps.com" }
    s.source       = { :git => "https://davidtravelstorysgps@bitbucket.org/travelstorys/white-label-ios", :tag => "#{s.version}" }
    s.public_header_files = "WhiteLabelBase/**/*.h"
    s.source_files = "WhiteLabelBase/**/*.[hm]"
    s.platform = :ios
    s.ios.deployment_target  = '8.0'
    s.dependency 'TravelStorysExtSDK'
    s.resources = ["WhiteLabelBase/**/*.storyboard"]
end